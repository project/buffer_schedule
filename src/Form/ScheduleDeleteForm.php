<?php

namespace Drupal\buffer_schedule\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Schedule entities.
 *
 * @ingroup buffer_schedule
 */
class ScheduleDeleteForm extends ContentEntityDeleteForm {


}
